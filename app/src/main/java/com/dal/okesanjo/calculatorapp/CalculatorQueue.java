package com.dal.okesanjo.calculatorapp;

public class CalculatorQueue {

    public String[] queue;
    public String operand;
    public String memory;

    public CalculatorQueue(){
        this.reset();
        this.memClr();
    }

    /* Helper functions */

    @Override
    public String toString(){
        return this.queue[0] + this.operand + this.queue[1];
    }

    /**
     *
     * @param q0
     * @param q1
     * @param operand
     * @return
     * @throws ArithmeticException
     */
    public String calculate(String q0, String q1, String operand) throws Exception {

        Float tmp;
        Float a = Float.parseFloat(q0);
        Float b = Float.parseFloat(q1);
        String op = operand.replace(" ", "");

        if (b == 0F && op.equals("\u00f7")) {
            throw new Exception();
        }

        if (op.equals("\u00f7")) {
            tmp = a / b;
        } else if (op.equals("\u00d7")) {
            tmp = a * b;
        } else if (op.equals("+")) {
            tmp = a + b;
        } else {
            tmp = a - b;
        }

        if (tmp == Math.round(tmp)) {
            return Integer.toString(Math.round(tmp));
        }

        return Float.toString(tmp);
    }

    /* Queue functions */

    /**
     * Reset the queue.
     */
    public void reset(){
        this.queue = new String[]{"", "0"};
        this.operand = "";
    }

    public String getPointer(){
        return this.queue[1];
    }

    public Boolean isPointerValid(){
        return (!this.queue[1].equals("") && !this.queue[1].equals("Error"));
    }

    /**
     * Append a numerical string to the current value.
     * @param no
     * @return
     */
    public String appendNo(String no){

        if (!this.queue[1].equals("0") && !this.queue[1].equals("Error")) {
            this.queue[1] = this.queue[1] + no;
        } else {
            this.queue[1] = no;
        }

        return this.toString();
    }

    /**
     * Move the current value down the queue.
     * @param
     * @return
     */
    public void shiftVal() throws Exception{

        if (!this.queue[0].equals("")){ // operate and shift
            try{
                this.queue[0] = this.calculate(this.queue[0], this.queue[1], this.operand);
                this.queue[1] = "";
            } catch (Exception e){
                this.queue[0] = "";
                this.queue[1] = "Error";
                throw new Exception();
            } finally {
                this.operand = ""; // remove operand after operation
            }
        } else{
            this.queue[0] = this.queue[1];
            this.queue[1] = "";
        }
    }

    /**
     *
     * @return
     */
    public String equals(){

        if(!this.operand.equals("") && this.isPointerValid()){
            try {
                this.queue[1] = this.calculate(this.queue[0], this.queue[1], this.operand);
            } catch (Exception e){
                this.queue[1] = "Error";
            } finally {
                this.queue[0] = "";
                this.operand = "";
            }
        }

        return this.toString();
    }

    /**
     * Append a operand string to the current value.
     * @param oprnd
     * @return
     */
    public String appendOp(String oprnd){

        if(this.isPointerValid()) {
            try {
                this.shiftVal();
                this.operand = " " + oprnd + " ";
            } catch (Exception e) {}
        }

        return this.toString();
    }

    /**
     *
     */
    public void tglSign(){
        if(this.operand.equals(" + ")) {
            this.operand = " - ";
        } else if(this.operand.equals(" - ")){
            this.operand = " + ";
        }else {
            if (this.isPointerValid()) {
                if (this.queue[1].contains("-")) {
                    this.queue[1] = this.queue[1].replace("-", "");
                }else {
                    this.queue[1] = "-" + this.queue[1];
                }
            }
        }
    }

    /* Memory functions */

    public void memRcl(){
        this.queue[1] = this.memory;
    }

    public void memAdd(){
        if(this.operand.equals("") && this.isPointerValid()){ // no computation
            try{
                this.memory = this.calculate(this.memory, this.queue[1], "+");
            } catch (Exception e){}
        }
    }

    public void memRmv(){
        if(this.operand.equals("") && this.isPointerValid()){
            try{
                this.memory = this.calculate(this.memory, this.queue[1], "-");
            } catch (Exception e){}
        }
    }

    public void memClr(){
        this.memory = "0";
    }

}
