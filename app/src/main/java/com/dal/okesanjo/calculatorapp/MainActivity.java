package com.dal.okesanjo.calculatorapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public CalculatorQueue queue;
    public TextView display_txt;
    public Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0;
    public Button btnMr, btnMp, btnMm, btnMc;
    public Button btnDiv, btnMul, btnAdd, btnSub, btnTgl, btnEql, btnClr, btnDot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queue = new CalculatorQueue();

        // grab all the layout items
        display_txt = findViewById(R.id.displayTxt);

        btn0 = findViewById(R.id.btn0);
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendNo("0"));
            }
        });

        btn1 = findViewById(R.id.btn1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendNo("1"));
            }
        });

        btn2 = findViewById(R.id.btn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendNo("2"));
            }
        });

        btn3 = findViewById(R.id.btn3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendNo("3"));
            }
        });

        btn4 = findViewById(R.id.btn4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendNo("4"));
            }
        });

        btn5 = findViewById(R.id.btn5);
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendNo("5"));
            }
        });

        btn6 = findViewById(R.id.btn6);
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendNo("6"));
            }
        });

        btn7 = findViewById(R.id.btn7);
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendNo("7"));
            }
        });

        btn8 = findViewById(R.id.btn8);
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendNo("8"));
            }
        });

        btn9 = findViewById(R.id.btn9);
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendNo("9"));
            }
        });

        btnDiv = findViewById(R.id.btnDiv);
        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendOp("\u00f7"));
            }
        });

        btnMul = findViewById(R.id.btnMul);
        btnMul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendOp("\u00d7"));
            }
        });

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendOp("+"));
            }
        });

        btnSub = findViewById(R.id.btnSub);
        btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.appendOp("-"));
            }
        });

        btnEql = findViewById(R.id.btnEql);
        btnEql.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display_txt.setText(queue.equals());
            }
        });

        btnClr = findViewById(R.id.btnClr);
        btnClr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.reset();
                queue.memClr();
                display_txt.setText(queue.toString());
            }
        });

        btnTgl = findViewById(R.id.btnTgl);
        btnTgl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.tglSign();
                display_txt.setText(queue.toString());
            }
        });

        btnDot = findViewById(R.id.btnDot);
        btnDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String val = queue.getPointer();
                if (!val.contains(".") && !val.equals("Error")) {
                    display_txt.setText(queue.appendNo("."));
                }
            }
        });

        btnMr = findViewById(R.id.btnMr);
        btnMr.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                queue.memRcl();
                display_txt.setText(queue.toString());
            }
        });

        btnMp = findViewById(R.id.btnMp);
        btnMp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                queue.memAdd();
            }
        });

        btnMm = findViewById(R.id.btnMm);
        btnMm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                queue.memRmv();
            }
        });

        btnMc = findViewById(R.id.btnMc);
        btnMc.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                queue.memClr();
            }
        });
    }
}
